# Experiment artifact for Specializing a Meta-Interpreter @ ManLang'18

This archive is intended to allow recreation of the experiments reported on.

## Contents of archive

`benchmarks` -- Tiger program workloads
`dynsem-variants` -- 5 DynSem and meta-interepreter variants
`tiger-variants` -- 3 specification variants of Tiger's dynamic semantics in DynSem
`results` -- collected raw data and plot generation
`m2-local` -- a Maven repository containing binary dependencies that are likely to become unavailable
`jdk` -- folder where Java Virtual Machine (1.8.0) should be placed (cannot be redistributed due to licensing)
`graalvm` -- folder where a Graal VM (1.0.0-rc1) should be placed (cannot be redistributed due to licensing)
`env.sh` -- environment exports
`all.sh` -- builds all variants and runs all experiments. places results in `results`
`graalvm-run.sh` -- runs a benchmark on the Graal VM
`jdk-run.sh` -- runs a benchmark on the Java VM


## Running experiments

This was tested to work on macOs.

1. Ensure you have an internet connection
2. Ensure you have Maven version >= 3.
3. Download Graal VM and decompress in `graalvm`
4. Download a Java Virtual Machine and decompress in `jdk`.
5. Run `sudo ./all.sh` -- sudo is required because `graalvm-run` and `jdk-run` use `nice` to give the benchmark higher scheduling priority
6. Results are collected in `results`

The naming scheme for result files is: `W_T_D_VM.txt`, where *W* is the workload name, *T* is the tiger variant name, *D* is the DynSem variant name and *VM* is either `jdk` or `graal`.
