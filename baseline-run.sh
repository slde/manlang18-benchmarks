#! /bin/sh

set -e

source env.sh

VARIANT=$1
WORKLOAD=$2
RESULTS_FILE=$3

export JAVA_HOME="$(pwd)/jdk/jdk1.8.0_144.jdk/Contents/Home"

TIGER_VARIANTDIR="$(pwd)/tiger-variants/${VARIANT}/lang.tiger.ninterpreter"

TIGER_JAR="$TIGER_VARIANTDIR/target/lang.tiger.ninterpreter-0.0.1-SNAPSHOT.jar"


# construct class path using maven
pushd $TIGER_VARIANTDIR
mvn dependency:build-classpath -s $MAVEN_SETTINGS -Dmdep.outputFile=cp.txt
popd

mkdir -p results

sudo nice -n -5 ${JAVA_HOME}/bin/java -Xss64m -Xms1g -Xmx1g -cp ${TIGER_JAR}:$(cat ${TIGER_VARIANTDIR}/cp.txt) org.metaborg.lang.tiger.ninterpreter.TigerInterpreter ${WORKLOAD} > results/${RESULTS_FILE} \
|| echo ">>>> RUN CRASHED <<<<"
