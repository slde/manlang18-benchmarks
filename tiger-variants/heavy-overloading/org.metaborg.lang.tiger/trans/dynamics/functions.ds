module dynamics/functions

imports ds-signatures/Functions-sig
imports dynamics/base
imports dynamics/store
imports dynamics/strings
imports dynamics/bindings
imports dynamics/natives
imports dynamics/numbers

signature
  sorts
  
  constructors
    ClosureV : List(FArg) * Exp * Env -> V
  arrows
    E |- funEnv(List(FunDec)) :: H --> Env :: H
    E |- evalFuns(List(FunDec)) :: H --> Env :: H       
    E |- evalArgs(List(FArg), List(Exp)) :: H --> Env :: H

rules // function definition

  FunDecs(fds) --> E
  where 
    funEnv(fds) --> E; 
    E |- evalFuns(fds) --> _ 
  
  E |- funEnv([]) --> E
  
  funEnv([FunDec(f : Id, _, _, _) | fds]) --> E
  where 
    E bindVar(f, UndefV()) |- funEnv(fds) --> E
  
  E |- evalFuns([]) --> E
  
  E |- evalFuns([FunDec(f : Id, args, _, e) | fds]) --> evalFuns(fds)
  where 
    writeVar(f, ClosureV(args, e, E)) --> _

rules // function call
  
  eval(Call(f : Id, args)) --> v
  where
    readVar(f) --> ClosureV(params, e, E);
    evalArgs(params, args) --> E';
    E {E', E} |- eval(e) --> v
    
  evalArgs([], []) --> {}
    
  evalArgs([FArg(x : Id, _) | args], [e | es]) --> {x |--> a, E}
  where 
    allocate(eval(e)) --> a; 
    evalArgs(args, es) --> E
    
rules // procedure definition
 
  funEnv([ProcDec(f : Id, _, _) | fds]) --> E
  where 
    E bindVar(f, UndefV()) |- funEnv(fds) --> E
 
  E |- evalFuns([ProcDec(f : Id, args, e) | fds]) --> evalFuns(fds)
  where 
    writeVar(f, ClosureV(args, e, E)) --> _
    