package org.metaborg.meta.lang.dynsem.interpreter.nodes.rules.dispatch;

import org.metaborg.meta.lang.dynsem.interpreter.nodes.rules.PremiseFailureException;
import org.metaborg.meta.lang.dynsem.interpreter.nodes.rules.RuleResult;

import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.dsl.Cached;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.source.SourceSection;

public abstract class NonTreeDispatchChainRoot extends DispatchChainRoot {

	private final String arrowName;
	private final Class<?> dispatchClass;

	@Child private DispatchChainRoot fallBack;

	public NonTreeDispatchChainRoot(SourceSection source, boolean failSoftly, String arrowname,
			Class<?> dispatchClass) {
		super(source, failSoftly);
		this.arrowName = arrowname;
		this.dispatchClass = dispatchClass;
	}

	@Specialization
	public RuleResult doCached(Object[] args, @Cached(value = "getTargets()", dimensions = 1) CallTarget[] targets) {
		for(int i = 0; i < targets.length; i++) {
			try {
				return (RuleResult) targets[i].call(args);
			}catch(PremiseFailureException pmfx) {
				;
			}
		}
		Class<?> nextClass = DispatchUtils.nextDispatchClass(args, dispatchClass);
		if (nextClass != null) {
			if (fallBack == null) {
				CompilerDirectives.transferToInterpreterAndInvalidate();
				fallBack = insert(
						NonTreeDispatchChainRootNodeGen.create(getSourceSection(), failSoftly, arrowName, nextClass));
			}

			return fallBack.execute(args);
		}

		throw PremiseFailureException.SINGLETON;
	}

	protected CallTarget[] getTargets() {
		return getContext().getRuleRegistry().lookupRules(arrowName, dispatchClass);
	}

}
