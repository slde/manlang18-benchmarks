module backend/interpreter/lists/matching

imports
  signatures/-
  signatures/dynsem/-
  backend/interpreter/-
  backend/interpreter/lists/util
  backend/utils/-
  backend/common/-
  libjava-front

strategies
  
  ds-to-interp-terms-matching-lists =
    map(require(ds-to-interp-terms-matching-list, debug-decl-name|"List match generation failed for"))

rules
  ds-to-interp-terms-matching-list:
    ls@ListSort(es) -> compilation-unit |[
        package ~x:<get-opt> MatchPkg();
       
        import ~x:$[[<get-opt> TermPkg()].*];
        import ~x:$[[<get-opt> MatchPkg()]].x_listmatchclassfactory.*;
        import ~x:$[[<get-opt> MatchPkg()]].x_listmatchclassfactory.x_listmatchclassnotailuninitng.*;
        import ~x:$[[<get-opt> MatchPkg()]].x_listmatchclassfactory.x_listmatchclasswithtailuninitng.*;
        import org.metaborg.lang.tiger.interpreter.generated.terms.match.List_A_Exp_Sort_MFactory.List_A_Exp_Sort_M_WithTail_UninitializedNodeGen.List_A_Exp_Sort_M_DeadHeadNodeGen;
        import org.metaborg.meta.lang.dynsem.interpreter.nodes.matching.MatchPattern;
        import org.metaborg.meta.lang.dynsem.interpreter.nodes.matching.NoOpPattern;
        import org.metaborg.meta.lang.dynsem.interpreter.nodes.matching.lists.ListLengthFixedMatch;
        import org.metaborg.meta.lang.dynsem.interpreter.nodes.matching.lists.ListLengthFixedMatchNodeGen;
        import org.metaborg.meta.lang.dynsem.interpreter.nodes.matching.lists.ListLengthLongerMatch;
        import org.metaborg.meta.lang.dynsem.interpreter.nodes.matching.lists.ListLengthLongerMatchNodeGen;
        
        import com.oracle.truffle.api.CompilerAsserts;
        import com.oracle.truffle.api.CompilerDirectives.CompilationFinal;
        import com.oracle.truffle.api.dsl.Specialization;
        import com.oracle.truffle.api.frame.VirtualFrame;
        import com.oracle.truffle.api.nodes.ExplodeLoop;
        import com.oracle.truffle.api.profiles.ConditionProfile;
        import com.oracle.truffle.api.source.SourceSection;
        
        public abstract class x_listmatchclass extends MatchPattern {
          
          public x_listmatchclass(SourceSection source) {
            super(source);
          }
        
          public static MatchPattern createUninitialized(SourceSection source, MatchPattern[] patterns,
              MatchPattern tailPattern) {
            if (tailPattern == null) {
              return x_listmatchclassnotailuninitng.create(source, patterns);
            } else {
              return x_listmatchclasswithtailuninitng.create(source, patterns, tailPattern);
            }
          }
        
          public static abstract class x_listmatchclassnotailuninit extends x_listmatchclass {
        
            @Children
            protected final MatchPattern[] elemPatterns;
        
            public x_listmatchclassnotailuninit(SourceSection source, MatchPattern[] patterns) {
              super(source);
              this.elemPatterns = patterns;
            }
        
            private static boolean elemPattsAreNoOps(MatchPattern[] elemPatterns) {
              CompilerAsserts.neverPartOfCompilation();
              for (MatchPattern patt : elemPatterns) {
                if (!(patt instanceof NoOpPattern)) {
                  return false;
                }
              }
              return true;
            }
        
            @Specialization
            public boolean executeSpecific(VirtualFrame frame, x_listclass l) {
              if (elemPattsAreNoOps(elemPatterns)) {
                return replace(ListLengthFixedMatchNodeGen.create(getSourceSection(), elemPatterns.length)).executeMatch(frame, l);
              } else {
                return replace(x_listmatchclassnotailng.create(getSourceSection(), elemPatterns)).executeMatch(frame, l);
              }
            }
        
            public static abstract class x_listmatchclassnotail extends x_listmatchclassnotailuninit {
        
              public x_listmatchclassnotail(SourceSection source, MatchPattern[] patterns) {
                super(source, patterns);
              }
        
              @Specialization
              @ExplodeLoop
              public boolean executeSpecific(VirtualFrame frame, x_listclass l) {
                if (l.size() != elemPatterns.length) {
                  return false;
                } else {
                  for (int idx = 0; idx < elemPatterns.length; idx++) {
                    if(!elemPatterns[idx].executeMatch(frame, l.head())) {
                      return false;
                    }
                    l = l.tail();
                  }
                  return true;
                }
              }
        
            }
        
          }
        
          public static abstract class x_listmatchclasswithtailuninit extends x_listmatchclass {
        
            @Children
            protected final MatchPattern[] elemPatterns;
        
            @Child
            protected MatchPattern tailPattern;
        
            public x_listmatchclasswithtailuninit(SourceSection source, MatchPattern[] patterns,
                MatchPattern tailPattern) {
              super(source);
              this.elemPatterns = patterns;
              this.tailPattern = tailPattern;
            }
        
            @CompilationFinal
            private boolean hasRun;
        
            @CompilationFinal
            private boolean elemPattsAreNoOps;
        
            private boolean elemPattsAreNoOps() {
              if (!hasRun) {
                hasRun = true;
                for (MatchPattern patt : elemPatterns) {
                  if (!(patt instanceof NoOpPattern)) {
                    elemPattsAreNoOps = false;
                    break;
                  }
                }
              }
              return elemPattsAreNoOps;
            }
        
            @Specialization
            public boolean executeSpecific(VirtualFrame frame, x_listclass l) {
              CompilerAsserts.neverPartOfCompilation("Should have already specialized. Untested code!");
              if (elemPattsAreNoOps()) {
                if (l.size() >= elemPatterns.length) {
                    boolean tailMatched = tailPattern.executeMatch(frame, l.drop(elemPatterns.length));
                    if (tailPattern instanceof NoOpPattern) {
                      replace(ListLengthFixedMatchNodeGen.create(getSourceSection(), elemPatterns.length));
                    } else if (tailPattern instanceof ListLengthFixedMatch) {
                      replace(ListLengthFixedMatchNodeGen.create(getSourceSection(), elemPatterns.length + ((ListLengthFixedMatch) tailPattern).getExpectedLength()));
                    } else if (tailPattern instanceof ListLengthLongerMatch) {
                      replace(ListLengthLongerMatchNodeGen.create(getSourceSection(), elemPatterns.length + ((ListLengthLongerMatch) tailPattern).getMinimalLength()));
                    } else {
                      replace(x_listmatchclassdeadheadng.create(getSourceSection(), elemPatterns.length, tailPattern));
                    }
                    return tailMatched;
                } else {
                  return false;
                }
              }  else {
                return replace(x_listmatchclassfullng.create(getSourceSection(), elemPatterns, tailPattern))
                    .executeSpecific(frame, l);
              }
            }
        
            public static abstract class x_listmatchclassfull extends x_listmatchclasswithtailuninit {
        
              public x_listmatchclassfull(SourceSection source, MatchPattern[] patterns,
                  MatchPattern tailPattern) {
                super(source, patterns, tailPattern);
              }
        
              @Specialization
              @ExplodeLoop
              public boolean executeSpecific(VirtualFrame frame, x_listclass l) {
                if (l.size() < elemPatterns.length) {
                  return false;
                } else {
                  x_listclass tail = l;
                  for(int idx = 0; idx < elemPatterns.length; idx++){
                    if(!elemPatterns[idx].executeMatch(frame, tail.head())){
                      return false;
                    }
                    tail = tail.tail();
                  }
                  return tailPattern.executeMatch(frame, tail);
                }
              }
        
            }

            public static abstract class x_listmatchclassdeadhead extends x_listmatchclass {
        
              private final int deadHeadSize;
        
              @Child
              private MatchPattern tailPattern;
        
              public x_listmatchclassdeadhead(SourceSection source, int deadHeadSize,
                  MatchPattern tailPattern) {
                super(source);
                this.deadHeadSize = deadHeadSize;
                this.tailPattern = tailPattern;
              }
        
              @Specialization
              public boolean executeSpecific(VirtualFrame frame, x_listclass l) {
                if (l.size() < deadHeadSize) {
                  return false;
                } else {
                  return tailPattern.executeMatch(frame, l.drop(deadHeadSize));
                }
              }
        
            }
        
          }
        }
    ]|
  where
    x_elemclass := <jclass-term> es;
    x_listclass := <jclass-term> ls;
    x_listmatchclass := <jclass-matcher> ls;
    x_listmatchclassnotailuninit := $[[x_listmatchclass]_NoTail_Uninitialized];
    x_listmatchclassnotailuninitng := $[[x_listmatchclassnotailuninit]NodeGen];
    x_listmatchclassnotail := $[[x_listmatchclass]_NoTail];
    x_listmatchclassnotailng := $[[x_listmatchclassnotail]NodeGen];
    x_listmatchclasswithtailuninit := $[[x_listmatchclass]_WithTail_Uninitialized];
    x_listmatchclasswithtailuninitng := $[[x_listmatchclasswithtailuninit]NodeGen];
    x_listmatchclassdeadhead := $[[x_listmatchclass]_DeadHead];
    x_listmatchclassdeadheadng := $[[x_listmatchclassdeadhead]NodeGen];
    x_listmatchclassfull := $[[x_listmatchclass]_Full];
    x_listmatchclassfullng := $[[x_listmatchclassfull]NodeGen];
    x_listmatchclassfactory := $[[x_listmatchclass]Factory]
    
