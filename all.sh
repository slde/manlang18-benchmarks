#! /bin/sh

source env.sh


set -e

function run_benchmarks {
  tiger=$1
  dynsem=$2
  on_graal=$3
  for workload in benchmarks/*.tig; do
      workloadname=$(basename $workload | cut -f 1 -d '.')
      echo "Running $workload on ds: $dynsem and tig:$1 on graal:$on_graal"
      if $on_graal; then
        result_file="${workloadname}_$1_$2_graal.txt"
        ./graalvm-run.sh $tiger $workload $result_file
      else
        result_file="${workloadname}_$1_$2_jdk.txt"
        ./jdk-run.sh $tiger $workload $result_file
      fi
  done
}

function build_dynsem {
  variant=$1
  pushd dynsem-variants/$variant
  pushd dynsem

  mvn -s $MAVEN_SETTINGS clean install
  popd
  pushd org.metaborg.meta.lang.dynsem.interpreter
  mvn -s $MAVEN_SETTINGS clean install
  popd
  popd
}

function build_tiger {
  variant=$1
  pushd tiger-variants/$variant
  pushd org.metaborg.lang.tiger.interpreter
  mvn -s $MAVEN_SETTINGS clean
  popd
  pushd org.metaborg.lang.tiger
  mvn -s $MAVEN_SETTINGS clean verify
  popd
  pushd org.metaborg.lang.tiger.interpreter
  mvn -s $MAVEN_SETTINGS verify
  popd
  popd
}

function build_baseline {
  variant=$1
  pushd tiger-variants/$variant
  pushd lang.tiger.ninterpreter
  mvn -s $MAVEN_SETTINGS clean verify
  popd
  popd
}

function run_baseline {
  tiger=$1
  dynsem="native"
  for workload in benchmarks/*.tig; do
      workloadname=$(basename $workload | cut -f 1 -d '.')
      echo "Running $workload on ds: $dynsem and tig: $tiger on graal: false"
      result_file="${workloadname}_${tiger}_${dynsem}_jdk.txt"
      ./baseline-run.sh $tiger $workload $result_file
  done
}

sudo echo "NOOP; just caching sudo rights"

# ---- Baseline (native on JVM with mutable env) ----
build_baseline "native-mutable-env"
run_baseline "native-mutable-env"

# ---- Baseline (native on JVM with persistent env) ----
build_baseline "native-persistent-env"
run_baseline "native-persistent-env"


# # ---- Regular DYNSEM ---
 DS="regular"
# build_dynsem $DS
#
# TIG="regular"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# run_benchmarks $TIG $DS false
#
# TIG="pure-loops"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# run_benchmarks $TIG $DS false
#
# TIG="heavy-overloading"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# run_benchmarks $TIG $DS false
#
# # ---- Dynamic matching DYNSEM ---
# DS="dynamic-matching"
# build_dynsem $DS
#
# TIG="regular"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# TIG="pure-loops"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# TIG="heavy-overloading"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# # ---- dispatch no caching DYNSEM ---
# DS="dispatch-no-cache"
# build_dynsem $DS
#
# TIG="regular"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# TIG="pure-loops"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# TIG="heavy-overloading"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# # ---- dispatch no trees DYNSEM ---
# DS="dispatch-no-trees"
# build_dynsem $DS
#
# TIG="regular"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# TIG="pure-loops"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# TIG="heavy-overloading"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# # ---- pure meta DYNSEM ---
# DS="pure-meta"
# build_dynsem $DS
#
# TIG="regular"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# TIG="pure-loops"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
#
# TIG="heavy-overloading"
# build_tiger $TIG
# run_benchmarks $TIG $DS true
# # run_benchmarks $TIG $DS false
