#! /bin/sh

set -e

source env.sh
export JAVA_HOME="$(pwd)/graalvm/graalvm-1.0.0-rc1/Contents/Home"

TIGER_VARIANTDIR="$(pwd)/tiger-variants/$1/org.metaborg.lang.tiger.interpreter"

TIGER_JAR="$TIGER_VARIANTDIR/target/org.metaborg.lang.tiger.interpreter-0.1.jar"
WORKLOAD=$2
RESULTS_FILE=$3

# construct class path using maven
pushd $TIGER_VARIANTDIR
mvn dependency:build-classpath -s $MAVEN_SETTINGS -Dmdep.outputFile=cp.txt
popd

mkdir -p results

BOOTCLASSPATH="-Xbootclasspath/a:\
$JAVA_HOME/jre/lib/truffle/truffle-api.jar:\
$JAVA_HOME/jre/lib/truffle/locator.jar:\
$JAVA_HOME/jre/lib/truffle/truffle-nfi.jar"

sudo nice -n -5 $JAVA_HOME/bin/java $BOOTCLASSPATH -Xss64m -Xms1g -Xmx1g \
-Dgraal.TruffleBackgroundCompilation=false \
-Dgraal.TruffleCompilationExceptionsArePrinted=false \
-Dgraal.TruffleCompilationThreshold=200 \
-Dgraal.TruffleInliningMaxCallerSize=10000 \
-Dgraal.TruffleSplittingMaxCalleeSize=100000 \
-cp $TIGER_JAR:$(cat $TIGER_VARIANTDIR/cp.txt) \
org.metaborg.lang.tiger.interpreter.generated.TigerMain $WORKLOAD > results/$3 \
|| echo ">>>> RUN CRASHED <<<<"
