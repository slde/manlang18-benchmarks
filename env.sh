#! /bin/sh

export MAVEN_OPTS="-server -Xms512m -Xmx1024m -Xss16m"

export BENCHMARKS=$(pwd)
export M2="$(pwd)/m2-local"
export MAVEN_SETTINGS="$M2/settings.xml"
export MAVEN_REPO="$M2/repository"
