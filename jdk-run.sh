#! /bin/sh

set -e

source env.sh

export JAVA_HOME="$(pwd)/jdk/jdk1.8.0_144.jdk/Contents/Home"

TIGER_VARIANTDIR="$(pwd)/tiger-variants/$1/org.metaborg.lang.tiger.interpreter"

TIGER_JAR="$TIGER_VARIANTDIR/target/org.metaborg.lang.tiger.interpreter-0.1.jar"
WORKLOAD=$2
RESULTS_FILE=$3

# construct class path using maven
pushd $TIGER_VARIANTDIR
mvn dependency:build-classpath -s $MAVEN_SETTINGS -Dmdep.outputFile=cp.txt
popd

mkdir -p results

sudo nice -n -5 $JAVA_HOME/bin/java -Xss64m -Xms1g -Xmx1g -cp $TIGER_JAR:$(cat $TIGER_VARIANTDIR/cp.txt) org.metaborg.lang.tiger.interpreter.generated.TigerMain $WORKLOAD > results/$3 \
|| echo ">>>> RUN CRASHED <<<<"
